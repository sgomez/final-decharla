# Generated by Django 4.1.7 on 2023-06-14 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("DeCharla", "0002_mensaje_imagenurl_alter_password_password_and_more"),
    ]

    operations = [
        migrations.RenameField(
            model_name="mensaje",
            old_name="imagenURL",
            new_name="imagen2",
        ),
        migrations.AddField(
            model_name="mensaje",
            name="is_image2",
            field=models.BooleanField(default=False),
        ),
    ]
