from django.db import models

class Sesion(models.Model):
    Sesion = models.TextField(unique=True)
    Nombre = models.TextField(default="anonimo")
    font_size = models.CharField(max_length=10, default="small")
    font_type = models.CharField(max_length=20, default="sans-serif")

class Sala(models.Model):
    Nombre = models.CharField(max_length=64)
    Autor = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    Votos = models.PositiveIntegerField(default=0)

class Mensaje(models.Model):
    Autor = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    Sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    Fecha = models.DateTimeField(auto_now_add=True)
    Texto = models.TextField()
    is_image2 = models.BooleanField(default=False)
    imagenURL = models.URLField(null=True, blank=True)

class Password(models.Model):
    Password = models.CharField(max_length=128)