from django.views.decorators.csrf import ensure_csrf_cookie
from django.core import validators
from django.shortcuts import render, HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.views.decorators.csrf import csrf_exempt
from .models import Sesion, Sala, Mensaje, Password
from django.http import HttpResponse
from django.utils import timezone
import xml.etree.ElementTree as ET
import random, string, json
import datetime


#añadimos imagen favicon
def poner_favicon(request):
    with open("favicon.png", "rb") as favicon: #abro en lectura binaria
        img = favicon.read() #leo la imagen
    return HttpResponse(img, content_type="image/png") #Devuelvo al navegador la imagen png

def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "true":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages

#Contará el número de salas activas/mensajes/imagenes de pie de pagina
def info_guardada():
    salas_activas = Sala.objects.count()
    mensajes = Mensaje.objects.filter(is_image2=False).count() #is_image=False porque sino lo detecta como imagen
    imagenes = Mensaje.objects.filter(is_image2=True).count()
    return salas_activas, mensajes, imagenes

def comprobar_sesion(request):
    cookie_sesion = request.COOKIES.get("Sesion") #mira la cookie asociada a la sesion
    if cookie_sesion is None: #Si no tengo la cookkie guardada creo una asociada a la nueva sesion
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))
        response = HttpResponseRedirect("/login")
        response.set_cookie("Sesion", cookie) #establezco la cookie a la sesion
        return response
    if request.path != "/login": #Si existe la cookie
        check = Sesion.objects.filter(Sesion=cookie_sesion).exists() #compruebo que tengo la cookie guardada asociada a la sesion
    else:
        check = True
    if not check: #si no existe cookie en la base de datos reedirijo al login
        return HttpResponseRedirect("/login")
    return None

#######################################################################################################
#funcion principal
@csrf_exempt
def index(request):
    sesion_ = comprobar_sesion(request) #compruebo la sesion del usuario
    if sesion_:
        return sesion_
    #si tengo esa sesion -->
    if request.method == "POST" or request.method == "GET":
        try:
            nombre = request.POST.get("sala")
            # obtengo el valor de 'sala' y compruebo si esta existe una sala asociada a ese nombre en la base de datos
            if not Sala.objects.filter(Nombre=nombre).exists():
                # si no la tengo, obtendre el objeto sesion asociado a la cookie de la sesion activa
                sesion_nueva = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
                sala_nueva = Sala(Nombre=nombre, Autor=sesion_nueva) #a la sala nueva asocio nombre de la sala y del chateador
                sala_nueva.save()
                return HttpResponseRedirect("/" + nombre)
        except Exception as e:
            print("Error:", str(e))
         #   return render(request, "error.html")
    salas_activas, mensajes, imagenes = info_guardada() #reuno los datos del chat total
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #tengo el objeto de la sesion de la cookie de sesion actual
    if sesion.Nombre:
        sesion_nombre = sesion.Nombre
    else:
        sesion_nombre = "Anonimo"
    context = { "lista_sala": lista_salas(request), "salas_activas": salas_activas, "mensajes": mensajes, "imagenes": imagenes,
                "sesion": sesion, "user_name": sesion_nombre}
    return render(request, "pagina_inicio.html", context)

#Un usuario se registra
@csrf_exempt
def registrar(request):
    try:
        if request.method == "GET" or request.method == "POST":
            contrasena = request.POST.get("password") #obtengo la contraseña introducida
            if Password.objects.filter(Password=contrasena).exists(): #si la contraseña esta ya en la base de datos
                # asocio esa sesion a la cookie de la sesion guardada
                sesion = Sesion()
                sesion.Sesion = request.COOKIES.get("Sesion")
                sesion.save()
                return HttpResponseRedirect("/") #reedirijo a la pag_inicio
        #compruebo el valor de la cookie de sesion de la solicitud
        cookie = request.COOKIES.get("Sesion")
        if Sesion.objects.filter(Sesion=cookie).exists(): #compruebo si existe con ese valor de cookie de sesion
            return HttpResponseRedirect("/")
    except Sesion.DoesNotExist: #no se da ningun caso --> error
        return HttpResponse(status=404)
    return render(request, "registro.html")

#cierro sesion
@csrf_exempt
def cerrar_sesion(request):
    if request.method == "POST":
        sesion_cookie = request.POST.get("cookie") #obtengo valor de la cookie de sesion
        if Sesion.objects.filter(Sesion=sesion_cookie).exists(): #compruebo que la cookie de esa sesion existe
            sesion = Sesion.objects.get(Sesion=sesion_cookie)
            sesion.delete() #la elimino y redirecciono a la pag_inicio
        redireccion = HttpResponseRedirect("/")
        redireccion.delete_cookie("Sesion")
        return redireccion

#######################################################################################################
@csrf_exempt
def configuracion(request):
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    if not sesion:
        return render("registro.html", request)
    if request.method == "POST":
        nombre = request.POST.get("name")
        sesion.Nombre = nombre
        font_size = request.POST.get("font_size")
        sesion.font_size = font_size
        font_type = request.POST.get("font_type")
        sesion.font_type = font_type
        sesion.save()
    salas_activas, mensajes, imagenes = info_guardada()
    configuration_html = render(request, "pagina_configuracion.html", { "salas_activas": salas_activas,"mensajes": mensajes,"sesion": sesion,
                                                                        "imagenes": imagenes, "lista_salas": lista_salas(request)})
    return configuration_html

#página de ayuda --> redigira a la pagina de ayuda
def seccion_ayuda(request):
    if request.method == "GET" or request.method == "POST":
        sesion_ = comprobar_sesion(request) #compruebo si tengo la cookie de la sesion
        if sesion_:
            return sesion_
        sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
        salas_activas, mensajes, imagenes= info_guardada()
        sala_lista = lista_salas(request)
        pagina_ayuda = render(request, "pagina_ayuda.html",{"lista_salas": sala_lista, "sesion": sesion, "salas_activas": salas_activas,
                                "mensajes": mensajes, "imagenes": imagenes})
        return pagina_ayuda

#######################################################################################################
#obtengo el listado de salas
def lista_salas(request):
    sesion_ = comprobar_sesion(request) #compruebo sesion
    if sesion_:
        return sesion_
    lista_salas = Sala.objects.all() #cojo todas las salas que tengo guardadas
    #compruebo la lista de salas que tengo guardada
    for sala_ in lista_salas:
        mensajes = Mensaje.objects.filter(Sala=sala_).count() #cuento el numero de mensajes de la sala inicial
        ultima_visita = request.session.get(f"ultima_visita{sala_.Nombre}") #valor de la ultima visita de la sesion en la que estoy
        if ultima_visita: #si la encuentro
            #contare todos los mensajes que se han podido enviar dsde la ultima visita a la pag
            if isinstance(ultima_visita, str):
                ultima_visita = datetime.datetime.strptime(ultima_visita, "%Y-%m-%d %H:%M:%S")  # Convertir a datetime
            ultimos_mensajes_vistos = Mensaje.objects.filter(Sala=sala_, Fecha__gt=ultima_visita).exclude(Autor__Sesion=request.COOKIES.get("Sesion")).count()
        else:
            ultimos_mensajes_vistos = mensajes #asigno los mensajes a variable de mensajes de la ultima visita
        sala_.mensajes = mensajes
        sala_.ultimos_mensajes_vistos = ultimos_mensajes_vistos
        #guardare estos datos nuevos con la fecha y hora del momento --> asocio clave para cada sala
        request.session[f"ultima_visita_{sala_.Nombre}"] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    return lista_salas

#función que redirige --> html listado_sala para buscar una sala
@ensure_csrf_cookie
def html_listado_salas(request):
    sesion_ = comprobar_sesion(request)
    if sesion_:
        return sesion_
    sala_no_existe = False
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #actualizo el valor de sesion actual
    if request.method == "POST":
        #cojo el valor del campo sala_nombre
        sala_nombre = request.POST.get("sala_nombre")
        if sala_nombre:
            try: #necesito comprobar que ese nombre coincide con Sala en la base de datos
                sala = Sala.objects.get(Nombre=sala_nombre)
                return HttpResponseRedirect(f'/{sala.Nombre}')
            except Sala.DoesNotExist:
                sala_no_existe = True

    salas_activas, mensajes, imagenes = info_guardada()
    return render(request, "listado_salas.html",{"sala_no_existe": sala_no_existe, "sesion": sesion, "salas_activas": salas_activas,
                    "mensajes": mensajes, "imagenes": imagenes})
####################################################################################################################################################################
@csrf_exempt
def crear_sala_nueva(request, id_sala):
    sala_ = comprobar_sesion(request)
    if sala_:
        return sala_
    redireccion = HttpResponseRedirect("/login")
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #cojo el valor actual de la sesion
    try:
        # compruebo si en la base de datos tengo algún objeto de Sala que coincida con el nombre_sala
        nombre_sala = Sala.objects.get(Nombre=id_sala)
        mensajes = Mensaje.objects.filter(Sala=nombre_sala).order_by('-Fecha')
        if request.method == "POST":
            texto = request.POST.get("mensaje")
            is_image2 = bool(request.POST.get('is_image2', False)) #compruebo el valor de is_image2
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion")) #cojo la sesion actual o creo una nueva
            mensaje = Mensaje(Texto=texto, is_image2=is_image2, Autor=autor, Sala=nombre_sala) #paso los nuevos valores a Mensaje
            if is_image2:
                if texto: #compruebo si tengo el valor para el campo mensaje
                    mensaje.imagenURL = texto
                    try:
                        validators.URLValidator()(mensaje.imagenURL)
                    except ValidationError:
                        return HttpResponse(status=404)
            mensaje.save()
        if request.method == "PUT":
            xml_string = request.body.decode('utf-8')
            xml_messages = xml_parser(xml_string)
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            for msg in xml_messages:
                mensaje = Mensaje(Texto=msg['text'], is_image2=msg['isimg'], Autor=autor, Sala=nombre_sala)
                mensaje.imagenURL = msg['text']
                mensaje.save()
        salas_activas, mensajes2, imagenes = info_guardada()
        sala_nueva = render (request, "sala_nueva.html", {"id_sala":id_sala, "salas_activas": salas_activas, "sesion":sesion, "mensajes": mensajes, "imagenes":imagenes,
                                                          "lista_salas": lista_salas(request)})
        return sala_nueva
    except Sala.DoesNotExist:
        return redireccion

@csrf_exempt
#en sala dinámica implementare el mismo codigo que en crear sala ya que la única modificación será que se recargue la página cada 30s
#para eso --> llamo al html de saladinamica.html
def sala_dinamica(request, id_sala):
    sala_ = comprobar_sesion(request)
    if sala_:
        return sala_
    redireccion = HttpResponseRedirect("/login")
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    try:
        nombre_sala = Sala.objects.get(Nombre=id_sala)
        mensajes = Mensaje.objects.filter(Sala=nombre_sala).order_by('-Fecha')
        if request.method == "POST":
            texto = request.POST.get("mensaje")
            is_image2 = bool(request.POST.get('is_image2', False))
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            mensaje = Mensaje(Texto=texto, is_image2=is_image2, Autor=autor, Sala=nombre_sala)
            if is_image2:
                if texto:
                    mensaje.imagenURL = texto
                    try:
                        validators.URLValidator()(mensaje.imagenURL)
                    except ValidationError:
                        return HttpResponse(status=404)
            mensaje.save()
        sala_nueva = render (request, "saladinamica.html", {"id_sala":id_sala, "sesion":sesion, "mensajes": mensajes, "lista_salas": lista_salas(request)})
        return sala_nueva
    except Sala.DoesNotExist:
        return redireccion

def sala_json(request, id_sala):
    response = comprobar_sesion(request)
    if response:
        return response
    try:
        sala_ = Sala.objects.get(Nombre=id_sala)
        #cojo los objetos de mensaje que estan asociados a la id_sala --> ordenador
        mensajes = Mensaje.objects.filter(Sala=sala_).order_by('-Fecha')
        json_mensajes = [] #guardo para iterar sobre esos mensajes(quiero los mensajes y el chateador) --> it
        for mensaje in mensajes:
            info_mensaje = {"author": mensaje.Autor.Nombre, "text": mensaje.Texto,
                "isimg": mensaje.is_image2, "date": mensaje.Fecha.strftime("%Y-%m-%d %H:%M:%S")}
            #añado a la lista el diccionario que acabo de crear para asociarlos
            json_mensajes.append(info_mensaje)
        return HttpResponse(json.dumps(json_mensajes), content_type="application/json")
    except Sala.DoesNotExist:
        return HttpResponse(status=404)
#######################################################################################################
@csrf_exempt
def borrar_sala(request, id_sala):
    try:
        sesion_ = comprobar_sesion(request)
        if sesion_:
            return sesion_
        #cojo objeto sala del id_sala y lo elimino
        sala = Sala.objects.get(id=id_sala)
        sala.delete()
        return HttpResponseRedirect("/")
    except ObjectDoesNotExist:
        return HttpResponseRedirect("/")

#Imlementacion para dar mg a una sala
def votar(request, id_sala):
    #cojo el objeto de la sesion actual que tendre asociado a su correspondiente cookie
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))
    if sesion:
        try: #quiero el objeto sala de la id_sala para sumar los mgg
            sala = Sala.objects.get(id=id_sala)
            sala.Votos += 1
            sala.save()
        except Exception as e:
            print("Error:", str(e))
    return HttpResponseRedirect("/")