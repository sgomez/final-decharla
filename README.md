# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Sara Gómez Civantos
* Titulación: Grado en Ingeniería Telématica
* Cuenta en laboratorios: sgomez
* Cuenta URJC: s.gomezc.2018@alumnos.urjc.es
* Video básico (url): https://youtu.be/SdtfJzSNcXE
* Video parte opcional (url): https://youtu.be/uv9QylV_D08
* Despliegue (url): https://sgomez.pythonanywhere.com/login
* Contraseñas: universidad / 1234
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria

Al iniciar nuestra página web, lo primero que solicitará la aplicación será ingresar una contraseña para acceder al chat. De esta manera, una vez que ingresamos en la página web, la contraseña quedará asociada a una cookie de sesión.

Una vez que hemos accedido, en la página principal se mostrará un listado con todas las salas creadas que contengan mensajes. Cada sala estará compuesta por un nombre que servirá como enlace a la página del chat, donde podremos enviar mensajes e imágenes. Además, se mostrará un recuento total de mensajes y el número de mensajes desde la última visita a la sala.

En la parte superior, tendremos una cabecera desde la cual se puede acceder a través de enlaces a las siguientes acciones:

Página principal: "Principal"
Página de configuración: "Configuración"
Página de ayuda: "Ayuda"
Página de acceso al Admin Site: "Admin"
Además, en la página de inicio habrá una opción disponible para buscar salas, donde podremos verificar si existe una sala con el nombre ingresado por el usuario. En caso de que exista, se redirigirá a la página de dicha sala, donde podremos enviar los mensajes e imágenes deseados. Si la sala no existe en la base de datos, se mostrará un mensaje indicando que la sala buscada no existe.

En el pie de página se incluirá un resumen de métricas de las salas activas, mensajes e imágenes de la web.

En la sección de "Configuración", podremos elegir el nombre que queremos que aparezca en la sesión de la página web, el cual se mostrará a la izquierda de la página principal. Además, podremos modificar el tipo de letra y su tamaño mediante las opciones disponibles en esta sección.

En la página de "Ayuda" se proporcionará información breve sobre el funcionamiento de la aplicación, documentación resumida y la autoría de la página web.

Dentro de una sala, para acceder a la sala dinámica, deberemos añadir "/din" a la URL, lo cual nos redirigirá a dicha sala. Del mismo modo, para acceder al formato JSON, debemos añadir "/json" a la URL.

## Lista partes opcionales

* Favicon: Se añadirá un favicon, que será la imagen que aparece en el buscador junto al nombre de la página.

* Cerrar sesión: Se incluirá un botón de "Cerrar sesión" que permitirá salir de la sesión actual y nos redirigirá a la página "/login".

* Borrar sesión: Se agregará un botón para borrar una sala, el cual eliminará la sala de la página principal.

* Me gusta: Se incluirá un botón de "Me gusta" que permitirá votar por las salas.
